package usda_template;

import gov.usda.nal.lci.template.importer.USFedLCATemplateImport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.openlca.app.Messages;
import org.openlca.app.db.Cache;
import org.openlca.app.db.Database;
import org.openlca.app.navigation.Navigator;
import org.openlca.app.rcp.ImageType;
import org.openlca.app.rcp.RcpActivator;
import org.openlca.app.wizards.io.FileImportPage;
//import org.openlca.io.xls.process.input.ExcelImport;
import org.openlca.io.UnitMapping;

import com.google.common.eventbus.EventBus;

public class ImportWizard extends Wizard implements IImportWizard {

	private FileImportPage importPage;
	private EventBus eventBus;

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("Import unit process from USDA Template");
//		setDefaultPageImageDescriptor(ImageType.IMPORT_ZIP_WIZARD
	//			.getDescriptor());
		//setDefaultPageImageDescriptor(RcpActivator.getImageDescriptor("icons/usda_logo.png"));
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		importPage = new FileImportPage(new String[] { "xlsx", "xls" }, true);
		addPage(importPage);
	}

	@Override
	public boolean performFinish() {
		File[] files = importPage.getFiles();
		if (files == null)
			return false;
		try {
			doRun(files);
			return true;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			Navigator.refresh();
			Cache.evictAll();
		}
	}

	private void doRun(final File[] files) throws Exception {
		getContainer().run(true, true, new IRunnableWithProgress() {
			@Override
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				monitor.beginTask(Messages.Import, files.length);
				UnitMapping mapping = UnitMapping.createDefault(Database.get());
				for (File file : files) {
					monitor.subTask(file.getName());
					System.out.println("IMPORTING FILE NAME :"+file.getName());
					USFedLCATemplateImport usdaTemplate;
					try {
						usdaTemplate = new USFedLCATemplateImport(
								Database.get(), new FileInputStream(file), mapping);
						usdaTemplate.setSource(file.getName());
						usdaTemplate.setEventBus(eventBus);
						System.out.println("RUNNING :"+file.getName());
						usdaTemplate.run();
					} catch (FileNotFoundException e) {						
						e.printStackTrace();
					}					
					monitor.worked(1);
				}
				monitor.done();
			}
		});

	}
}
